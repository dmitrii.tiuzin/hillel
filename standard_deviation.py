import requests


def main(n=0):
    f = requests.request('GET', url='https://raw.githubusercontent.com/varelaz/python-course/master/03-git/hw.csv')
    res = f.text
    lst = []

    for line in res.split('\n')[1:-1]:
        col = list(map(float, line.split(', ')))
        lst.append(col[n])

    len_ = len(lst)
    mid = sum(lst) / len_
    disp = sum([j ** 2 for j in [i - mid for i in lst]]) / len_

    return disp ** 0.5


if __name__ == '__main__':
    print(main(n=1))
    print(main(n=2))
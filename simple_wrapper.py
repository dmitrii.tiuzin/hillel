import functools


def log_function_call(text):
    def bar(foo):
        @functools.wraps(foo)
        def wrapper(*args, **kwargs):
            res = foo(*args, **kwargs)
            print(f'Function {text} called with arguments: {args}, {kwargs}, result: {res}')
            return res
        return wrapper
    return bar


@log_function_call('f')
def f(a, b, name):
    return a + b + 9


call = f(1, 2, name='value')



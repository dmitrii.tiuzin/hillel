import platform
import time
import random
import functools

digit = random.randint(1, 5)
op_sys = platform.system(), platform.release()


def print_message(msg):
    def log_function_call(foo):
        @functools.wraps(foo)
        def wrapper(*args, **kwargs):
            start = time.time()
            iterations = foo(*args, **kwargs)
            end = time.time()
            print(msg , end - start, 'seconds')
            return iterations
        return wrapper
    return log_function_call


@print_message('Process was taking during')
def simple_diagnostic(n, system = 'Your system', release='on this PC'):
    print(system, 'works without any updates')
    lst = []
    for i in range(n * 100000000):
        lst.append(i)
    return len(lst)


@print_message('Process was taking during')
def after_update(n, system = 'Your system', release='on this PC'):
    print(system, release, 'has been updated with app')
    lst = [i for i in range(n * 100000000)]
    return len(lst)


result1 = simple_diagnostic(digit)
time.sleep(3)  # imitation of CPU's work
result2 = after_update(digit, *op_sys)

print('Capasity of diagnostic:', '{:,}'.format(result1 + result2), 'iterations', '\nClearing all updates...')
time.sleep(3)  # imitation of CPU's work


